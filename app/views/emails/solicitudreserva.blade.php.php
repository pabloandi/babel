@extends('opac')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class='panel-title'>Ejemplares para solicitar reserva</h4>
    </div>
    <p>
        El usuario <br>
        Número de cuenta : {{{ $usuario->no_cuenta }}} <br>
        Nombre: {{{ $usuario->nombre }}} <br>
        Correo: {{{ $usuario->correo }}} <br>
        Ha solicitado la reserva del siguiente material:
    </p>
   <ul class='list-group'>
        @foreach($ejemplares as $ejemplar)
            <li class='media ejemplar' id="ejemplar-{{{ $ejemplar->getPk() }}}">
                @include('opac._ejemplar',array('ejemplar'=>$ejemplar))
            </li>
        @endforeach
    </ul>
</div>

@stop