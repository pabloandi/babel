<div class="row">
    <div class="col-md-8">
        <div >
            <strong>Título: </strong>{{{ $prestamo->titulo }}}
        </div>
        <div >
            <strong>Autor: </strong>{{{ $prestamo->autor }}}
        </div>
        <div >
            <strong>Fecha préstamo: </strong>{{{ $prestamo->fechaprestamo }}}
        </div>
        <div >
            <strong>Fecha entrega:</strong>{{{ $prestamo->fechaentrega }}}
        </div>
        <div >
            <strong>Tipo de préstamo:</strong>{{{ $prestamo->tipoprestamo }}}
        </div>
        <div >
            <strong>Biblioteca:</strong>{{{ $prestamo->labiblioteca->nombre }}}
        </div>
    </div>
    <div class="col-md-4">
        @include('opac.partials.prestamos._opciones_detalle_prestamo',array('prestamo'=>$prestamo))
    </div>
</div>
