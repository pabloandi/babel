<div class="panel panel-default">
    <div class="panel-heading">Detalles de usuario</div>
         
    <div class="panel-body" id="infousuario-detalleusuario" >
        @include('opac.partials.usuarios._detalle_usuario',array('usuario'=>$usuario))
    </div>
            
</div>
                


@if($usuario->prestamos())
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class='panel-title'>Préstamos</h4>
        </div>
        
        <ul class='list-group'>
        @foreach($usuario->prestamos()->get() as $prestamo)
            <li class='list-group-item'>
                @include('opac.partials.prestamos._detalle_prestamo',array('prestamo'=>$prestamo))
            </li>
        @endforeach
        </ul>
    </div>    
@endif

@if($usuario->multas())
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class='panel-title'>Multas</h4>
        </div>
        <ul class='list-group'>

        @foreach($usuario->multas()->get() as $multa)
            <li class='list-group-item'>
                @include('opac.partials.multas._detalle_multa',array('multa'=>$multa))
            </li>
        @endforeach
        </ul>
    </div>

@endif