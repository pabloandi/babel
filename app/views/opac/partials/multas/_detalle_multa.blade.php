<div class="row">
    <div class="col-md-8">
        <div><strong>Fecha devolución:</strong>{{{ $multa->fechadevolucion }}}</div>
        <div><strong>Monto:</strong>{{{ money_format('%i',$multa->monto) }}}</div>
        <div><strong>Observaciones:</strong>{{{ $multa->observaciones }}}</div>
    </div>
    <div class="col-md-4">
        @include('opac.partials.multas._opciones_detalle_multa',array('multa'=>$multa))
    </div>
</div>