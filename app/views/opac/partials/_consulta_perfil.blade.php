{{

    Form::jqRemoteForm(array(
                                'update' => array('success' => 'consulta_perfil', 'failure' => "$('#boton-buscar-perfil').button('reset'); $('#perfil-loader').hide(); alert('no existe el usuario');"),
                                'url' => URL::action('OPACController@getInfoUsuario'),
                                'condition' => '$("#codigo_usuario").val()!=""',
                                'before' => '$("#consulta_perfil").html("")',
                                'loading' => '$("#boton-buscar-perfil").button("loading")',
                                "success" => "$('#boton-buscar-perfil').button('reset');
                                                bootbox.dialog({
                                                    message: data,
                                                    title: 'Consulta perfil',
                                                  });
                                                
                                            ",
                                "method" => "get",
                                "with"  =>  "'codigo_usuario='+$('#codigo_usuario').val()"
                                    )
                            ,array("class" => "navbar-form navbar-right"))

}}

<div class="input-group">
    <input type="password" class="form-control" name="codigo_usuario" id="codigo_usuario" placeholder="Numero de cuenta ej: 123456" />
    <span class="input-group-btn">
        <button id="boton-buscar-perfil" type="submit" class="btn btn-default ">
            <span class='glyphicon glyphicon-user'></span>Mi perfil
        </button>
    </span>
</div>
{{ Form::close() }}