<div class="modal fade" id="modal-form-createuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Formulario de creación de usuario</h4>
      </div>
      <div class="modal-body">
          <p>Para poder realizar un préstamo interbibliotecario, se debe hacer el registro de datos del usuario en el sistema</p>
          <div id="formcreateuser-problems"></div>
        <form >
          <div class="form-group">
            <label for="formcreateuser-id">Identificación</label>
            <input type="text" class="form-control" id="formcreateuser-id" placeholder="ID">
          </div>
          <div class="form-group">
            <label for="formcreateuser-idtype">Tipo de Identificación</label>
            <select type="text" class="form-control" id="formcreateuser-idtype">
                <option value="CC">Cédula Ciudadanía</option>
                <option value="TI">Tarjeta identidad</option>
                <option value="PP">Pasaporte</option>
                <option value="NIT">NIT</option>
          </div>
          <div class="form-group">
            <label for="formcreateuser-agency">Institución</label>
            <input type="text" class="form-control" id="formcreateuser-agency" placeholder="ej: Universidad del Valle">
          </div>
          <div class="form-group">
            <label for="formcreateuser-name">Nombre</label>
            <input type="text" class="form-control" id="formcreateuser-name" placeholder="Nombre">
          </div>
          <div class="form-group">
            <label for="formcreateuser-email">Email </label>
            <input type="email" class="form-control" id="formcreateuser-email" placeholder="ej: usuario@ejemplo.com">
          </div>
          <div class="form-group">
            <label for="formcreateuser-address">Dirección física </label>
            <input type="text" class="form-control" id="formcreateuser-address" placeholder="ej: Calle 24 # 21-47">
          </div>
          <div class="form-group">
            <label for="formcreateuser-datebirth">Fecha de nacimiento</label>
            <input type="text" class="form-control" id="formcreateuser-datebirth" placeholder="Fecha de nacimiento">
          </div>
          <input type="hidden" id="formcreateuser-endpoint" value=""/>
        </form>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="doCreateUser()">Enviar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-form-askuserid" tabindex="-1" role="dialog" aria-labelledby="modal-form-askuserid-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-form-askuserid-label">Formulario de creación de usuario</h4>
      </div>
      <div class="modal-body">
          <p>Para poder realizar un préstamo interbibliotecario, se debe hacer el registro de datos del usuario en el sistema</p>
          <div id="formcreateuser-problems"></div>
        <form >
          <div class="form-group">
            <label for="formcreateuser-id">Identificación</label>
            <input type="text" class="form-control" id="formcreateuser-id" placeholder="ID">
          </div>
          <div class="form-group">
            <label for="formcreateuser-idtype">Tipo de Identificación</label>
            <select type="text" class="form-control" id="formcreateuser-idtype">
                <option value="CC">Cédula Ciudadanía</option>
                <option value="TI">Tarjeta identidad</option>
                <option value="PP">Pasaporte</option>
                <option value="NIT">NIT</option>
          </div>
          <div class="form-group">
            <label for="formcreateuser-agency">Institución</label>
            <input type="text" class="form-control" id="formcreateuser-agency" placeholder="ej: Universidad del Valle">
          </div>
          <input type="hidden" id="formcreateuser-endpoint" value=""/>
        </form>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="doCreateUser()">Enviar</button>
      </div>
    </div>
  </div>
</div>
