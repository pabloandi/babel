<div class="row">
    <div class="col-md-4">
        <strong>Código</strong>
    </div>
    <div class="col-md-4">{{ $usuario->no_cuenta }}</div>
</div>
<div class="row">
    <div class="col-md-4">
        <strong>Plan de estudio</strong>
    </div>
    <div class="col-md-4">{{ $usuario->laescuela->nombre }}</div>
</div>
<div class="row">
    <div class="col-md-4">
        <strong>Grupo</strong>
    </div>
    <div class="col-md-4">{{ $usuario->elgrupo->descripcion }}</div>
</div>
@if($usuario->multas)
<div class="row">
    <div class="col-md-4">
        <strong>Total multas</strong>
    </div>
    <div class="col-md-4">{{ $usuario->totalmultas() }}</div>
</div>
@endif


