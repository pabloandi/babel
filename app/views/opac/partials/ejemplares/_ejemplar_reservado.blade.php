<div class='row'>
    <div class='col-md-8'>
        @include('opac.partials.ejemplares._ejemplar',array('ejemplar'=>$ejemplar))
    </div>
    <div class='col-md-4'>

            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>

            <a href="javascript:;" class="btn btn-default rechazar-ejemplar" data-ejemplar="{{ trim($ejemplar->itemid) }}" >Rechazar este ejemplar</a>

           {{ Form::hidden('ejemplares_a_reservar[]',json_encode($ejemplar)) }}
        </div>
</div>
