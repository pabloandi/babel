<?php

namespace Pagination\Presenters;

class SbwebPresenter extends \Illuminate\Pagination\Presenter {

    public function getActivePageWrapper($text)
    {
        return '<li class="active"><a href="#">'.$text.'</a></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="disabled"><a href="#">'.$text.'</a></li>';
    }

    public function getPageLinkWrapper($url, $page, $rel = null)
    {
        $id="enlace-paginador-". \Illuminate\Support\Str::quickRandom(8);
        return sprintf("<li>%s</li>", \Illuminate\Support\Facades\Form::jqRemoteLink($page,array(
            'url'       =>  $url,
            'loading'   =>  "jQuery('#$id').button('loading')",
            'success'  =>  "jQuery('#$id').button('reset'); jQuery('#$id').closest('.pagina-ajax').html(data.datos);",
            'method'    =>  "get",
            'dataType'  =>  'json',
            
        ),
        array(
            "id"                =>  $id,
            "data-loading-text" =>  "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>"
        )));
        
    }

}

?>
