<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscuelaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('escuela', function(Blueprint $table)
		{
			

                        $table->engine = 'MyISAM';

                        $table->bigInteger('idescuela');
                        $table->string('nombre',250);
                        $table->string('nombrecorto',50)->nullable();
                        $table->string('responsable',250)->nullable();
                        $table->string('correo',50)->nullable();
                        $table->bigInteger('biblioteca');

                        $table->primary('idescuela');
                        $table->foreign('biblioteca')->references('idbiblioteca')->on('biblioteca');

                        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('escuela'))
		Schema::drop('escuela');
	}

}
