<?php

class Ejemplar extends Eloquent{
    
    protected $table='ejemplar';


    public function labiblioteca(){
        return $this->hasOne('Biblioteca','idbiblioteca','biblioteca');
    }
    
    public function eltipomaterial(){
        return $this->hasOne('Tipomaterial','idtipomaterial','tipomaterial');
    }
    
    public function laficha(){
        return $this->belongsTo('Ficha','ficha','idficha');
    }
    
    public function lareserva(){
        return $this->belongsTo('Reserva','no_cuenta','no_cuenta');
    }

    public function estado(){
        return $this->lareserva ? 'reservado' : 'disponible';
        
    }
}

?>
