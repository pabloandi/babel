<div class="list-group" id='acordeon-ayuda'>
  <a data-toggle='collapse' data-parent='acordeon-ayuda' href="#ayuda-1" class="list-group-item">
    <h4 class="list-group-item-heading">Búsqueda bibliográfica</h4>
    <p class="list-group-item-text">En este video le explicaremos como buscar en el catálogo</p>
  </a>
  <div id='ayuda-1' class='collapse'>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" width="560" height="315" src="//www.youtube.com/embed/vygFmUf8d6I" frameborder="0" allowfullscreen></iframe>
        </div>
  </div>
  <a data-toggle='collapse' data-parent='acordeon-ayuda' href="#ayuda-2" class="list-group-item">
    <h4 class="list-group-item-heading">Reserva del material documental</h4>
    <p class="list-group-item-text">En este video le explicaremos como buscar en el catálogo</p>
  </a>
    
  <div id='ayuda-2' class='collapse'>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" width="560" height="315" src="//www.youtube.com/embed/YsBEgn5CF0w" frameborder="0" allowfullscreen></iframe>
        </div>
  </div>
  <a data-toggle='collapse' data-parent='acordeon-ayuda' href="#ayuda-3" class="list-group-item">
    <h4 class="list-group-item-heading">Detalles de usuario</h4>
    <p class="list-group-item-text">En este video le explicaremos como buscar en el catálogo</p>
  </a>
    
  <div id='ayuda-3' class='collapse'>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" width="560" height="315" src="//www.youtube.com/embed/1ZUcDg9m74o" frameborder="0" allowfullscreen></iframe>
        </div>
  </div>
  <a data-toggle='collapse' data-parent='acordeon-ayuda' href="#ayuda-4" class="list-group-item">
    <h4 class="list-group-item-heading">Buzón de comentario</h4>
    <p class="list-group-item-text">En este video le explicaremos como buscar en el catálogo</p>
  </a>
    
  <div id='ayuda-4' class='collapse'>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" width="560" height="315" src="//www.youtube.com/embed/1ZUcDg9m74o" frameborder="0" allowfullscreen></iframe>
        </div>
  </div>
</div>


    
