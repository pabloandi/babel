<div class='row'>
    <div class='col-md-8'>
        @include('opac.partials.ejemplares._ejemplar',array('ejemplar'=>$ejemplar))
    </div>

    @if($ejemplar->estado()=='disponible' && !(preg_match("/^(r|R)+/", trim($ejemplar->laficha->clasificacion) ) ) === true )

    <div class='cold-md-4'>
        <span id='ejemplar-carrito' class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
       {{
            Form::jqRemoteLink('Agregar ejemplar al carrito',
                array(
                    "url"       =>  URL::action("OPACController@getAgregarEjemplares"),
                    "loading"   =>  "jQuery('#solicitar-ejemplar-".$ejemplar->idejemplar."').button('loading')",
                    "script"    =>  'true',
                    "success"   =>  "
                                    jQuery('#solicitar-ejemplar-".$ejemplar->idejemplar."').button('reset');
                                    jQuery('#solicitar-ejemplar-".$ejemplar->idejemplar."').after('<span class=\'glyphicon glyphicon-ok\' aria-hidden=\'true\'></span>  Agregado');
                                    jQuery('#solicitar-ejemplar-".$ejemplar->idejemplar.", #ejemplar-carrito').remove();
                                    agregarEjemplarCarro();

                                    ",
                    "method"    =>  "get",
                    "with"      =>  "'ejemplar_solicitado='+".$ejemplar->idejemplar
                ),
                array(
                    "id"=>"solicitar-ejemplar-".$ejemplar->idejemplar,
                    "data-loading-text" => "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>"
                    )
            )
       }}


    </div>
    @endif
</div>