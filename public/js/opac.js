
function agregarEjemplarCarro(){
    carro=jQuery("#carrito-compra");
    var cantidad=parseInt(carro.text());
    carro.text(cantidad+1);
}

function agregarFaceta(faceta,valor){

    jQuery("#filtro_facetas").append(
            '<input id="filtro-faceta-'+faceta.replace(/\s+/g, '')+'" type="hidden" name="filtros[AND][]" value="'+faceta.replace(/\s+/g, '')+':'+valor+'" />'
        );
    jQuery("#form_busqueda_principal").submit();
}

function removerFaceta(faceta){

    jQuery('#filtro-faceta-'+faceta.replace(/\s+/g, '')).remove();
    jQuery("#form_busqueda_principal").submit();
}

function actualizarFiltroAdicional(elemento){
    var operacion=jQuery(elemento).closest(".form-inline").find(".filtro-operacion :selected")[0].value;
    var categoria=jQuery(elemento).closest(".form-inline").find(".filtro-categoria :selected")[0].value;
    jQuery(elemento).closest(".form-inline").find(".filtro-caja")[0].name='filtros['+operacion+':'+categoria+'][]';

}

function mostrarEtiquetasmarc(ficha,id){
    div=jQuery("#"+id);
    for(var key in ficha){
        if(ficha.hasOwnProperty(key)){
            row="<div class='row'><div class='col-md-4'>"+key+"</div><div class='col-md-8'>"+ficha[key]+"</div></div>";
            div.append(row);
        }
    }
}


$.dialogs = {};

$.dialogs.dialog_crearusuario = function(){
    var dfd = $.Deferred();

    var form_createuser_description = `
        <p>No se encontró su identificación en una de las bibliotecas. Puede llenar datos de usuario para crear un registro en la biblioteca donde va a enviar la solicitud de préstamo</p>
        <div id='form_createuser'></div>
        `;

    var dialog = bootbox.dialog({
        show: true,
        message: form_createuser_description,
        buttons: {
            enviar: {
                label: "Enviar",
                className: "btn btn-raised btn-info",
                callback: function(){
                    if($("#form_createuser").alpaca().isValid(true)){
                        var user_data = $("#form_createuser").alpaca().getValue();
                        $.user_id = user_data.useridentifiervalue;
                        dfd.resolve(user_data);

                    }
                    else{
                        // alert("Valor no aceptable: " + JSON.stringify(val, null, "  "));
                        return false;
                    }
                }
            }
        }


    });

    dialog.init(function(){
        $("#form_createuser").alpaca({
            schema: {
                title: "Formulario envio de datos de usuario",
                description: "",
                type: "object",
                properties: {
                    useridentifiervalue: {
                        type: "string",
                        title: "Identificación",
                        required: true,
                        default: $.user_id
                    },
                    agencyid: {
                        type: "string",
                        title: "Institución",
                        required: true
                    },
                    givenname: {
                        type: "string",
                        title: "Nombres",
                        required: true
                    },
                    surname: {
                        type: "string",
                        title: "Apellidos",
                        required: true
                    },
                    electronicaddress: {
                        format: "email",
                        required: true
                    }

                }
            },
            options: {
                hideInitValidationError: true,
                fields: {
                    agencyid:{
                        helper: "Nombre de la institución a la que acude. Ej: Universidad del Valle"
                    },
                    electronicaddress: {
                        label: "Email"
                    }

                }
            },
            view: {
                locale: "es_ES"
            }
        });
    });

    return dfd.promise();
}

$.dialogs.dialog_reserva = function(data){

    var dfd = $.Deferred();
    bootbox.dialog({
        message: data,
        show: true,
        buttons: {
           reservar: {
               label: 'Reservar',
               className: "btn btn-raised btn-info",
               callback: function(){
                   if($("#reserva-codigo-usuario").val()!=''){

                       var form_userid = $("#reserva-codigo-usuario").val();
                       var form_reserve_data = $("#form-solreserva-ejemplares").serializeArray();
                       $.user_id = form_userid;
                       $.reserved_items = jQuery.map(form_reserve_data, function(item){
                           return jQuery.parseJSON(item.value);
                       });

                       var form_data = {
                           userid: form_userid,
                           reserved_items: $.reserved_items
                       };

                       dfd.resolve(form_data);


                   }
                   else{
                       return false;
                   }
               } // end callback
           }
       }
    });

    return dfd.promise();
}

jQuery(function(){

        $(document).on("click", ".rechazar-ejemplar", function(){
            var ejemplar = $(this).data("ejemplar");
            bootbox.confirm({
              size: "small",
              message: "Esta seguro de rechazar este ejemplar?",
              callback: function(result){
                  if(result){

                      $.ajax({
                          url: 'opac/rechazar-ejemplar',
                          type: 'GET',
                          data: "ejemplar_rechazado="+ejemplar

                      })
                      .done(function(){
                          $("#ejemplar-reservado-"+ejemplar).remove();
                      });
                  }
              }
          });

        });

         jQuery.ajaxSetup({
             cache: false
         });

        bootbox.setDefaults({
            locale: "es",
            closeButton: true,
        });

         $('.leamas').load(function(){
             $('.leamas').readmore({
                speed: 100,
                collapsedHeight: 200,
                heightMargin: 16,
                moreLink: '<a href="#">Lea más</a>',
                lessLink: '<a href="#">Cerrar</a>',
                embedCSS: true,
                blockCSS: 'display: block; width: 100%;',
                startOpen: false,

                // callbacks
                beforeToggle: function(){},
                afterToggle: function(){}
            });
         });

         jQuery("#boton-reserva").click(function(){

             var getEjemplaresReservadosData = $.get('/opac/ejemplares-reservados',{
                 beforeSend: function(){
                     $('#boton-reserva').button('loading');
                 }
             }).done(function(response){

                 var flow = new $.Deferred();

                 flow.then(function(data){
                     console.log("generating reservation dialog");
                     return $.dialogs.dialog_reserva(data);
                 })
                 .then(function(data){
                     console.log("checking user on the endpoints");
                     $.unknow_user_endpoints = [];
                     return $.when.apply(null, $.map( data.reserved_items, function(item){
                         return $.doLookupUser(item.endpoint, item);
                     })).then(
                         function(){
                             return $.Deferred().resolve(data);
                         },
                         function(){
                             return $.Deferred().reject(data);
                         }
                    );
                 })
                 .then(
                     function(data){
                         console.log("user know in every library, continuing ...");
                         return $.Deferred().resolve(data);
                     },
                     function(data){
                         console.log("user unknow in some libraries, atempt to create user");
                         return $.when.apply(null, $.map( $.unknow_user_endpoints, function(endpoint){
                             return $.dialogs.dialog_crearusuario().done(function(user_data){
                                 $.doCreateUser(endpoint, user_data);
                             });

                         })).then(
                             function(){
                                 console.log('success in creating users in endpoints');
                                 return $.Deferred().resolve(data);
                             },
                             function(){
                                 console.log('failed at creating users in endpoints');

                             }
                        );
                     }
                 )
                 .then(
                    function(data){
                        console.log("requesting item ...");
                        return $.when.apply(null,$.map($.reserved_items, function(item){
                            item.userid = $.user_id;
                            console.log("requesting item : "+item);
                            return $.doRequestItem(item.endpoint, item);
                        })).then(
                            function(){
                                $.get('/opac/flush-session', function(){

                                })
                                .done(function(){
                                    console.log("done requesting items");
                                    info("La solicitud de préstamo ha sido exitosa.");
                                });

                            },
                            function(){
                                console.log("failed requesting items ...");
                            }
                        )
                    },
                    function(data){
                        console.log("failed in requesting item ...");
                    }
                );

                 flow.resolve(response);



             }).fail(function(){
                 alert('no existen ejemplares solicitados');
             }).always(function(){
                 $('#boton-reserva').button('reset');
             });


         });



});
