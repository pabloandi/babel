<?php

use App\Events;

Event::listen('estadisticas.tags','EstadisticasHandler@tags');

Event::listen('estadisticas.busquedas_exitosas','EstadisticasHandler@busquedas');

Event::listen('estadisticas.busquedas_fallidas','EstadisticasHandler@busquedas');

Event::listen('estadisticas.visitas','EstadisticasHandler@visitas');

Event::listen('estadisticas.libromasconsultado','EstadisticasHandler@libromasconsultado');

?>
