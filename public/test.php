<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>    
<body>
<?php

// Server URL with port and database descriptor
                //$server = "z3950.obvsg.at:9991/ACC01";
                //$syntax = "mab";
                // for USMARC example uncomment next $server and $syntax defs
                 $server = "ticuna.banrep.gov.co:7090/ABNET_DB";
                 //$server = "metaproxy:9000";
                 $syntax = "marc21";
                // Mapping CCL keywords to Bib-1-Attributes (allows convenient query formulation)
                $fields = array("titulo" => "1=4",
                                "ibn" => "1=7",
                                "isn"   => "1=8",
                                "wja"   => "1=31",
                                "autor"   => "1=1003",
                                "wve"   => "1=1018");
                // establish connection and store session identifier,
                // credentials are an optional second parameter in format "<user>/<passwd>"
                //$session = yaz_connect($server,array('charset'=>'UTF-8'));
                $session = yaz_connect($server);
                // check whether an error occurred
                if (yaz_error($session) != ""){
                    die("Error: " . yaz_error($session));
                }
                
                //yaz_database($session,"blaa+javeriana+loc");
                        
                // configure desired result syntax (must be specified in Target Profile)
                yaz_syntax($session, $syntax);
                // configure YAZ's CCL parser with the mapping from above
                yaz_ccl_conf($session, $fields);
                // define a query using CCL (allowed operators are 'and', 'or', 'not')
                // available fields are the ones in $fields (again see Target Profile)
                $ccl_query = "(titulo = Muerte) and (autor = Gabriel)";
                // let YAZ parse the query and check for error
                if (!yaz_ccl_parse($session, $ccl_query, $ccl_result)){
                        die("The query could not be parsed.");
                } else{
                    // fetch RPN result from the parser
                    $rpn = $ccl_result["rpn"];
                    yaz_range($session, 1, 20);
                    // do the actual query
                    yaz_search($session, "rpn", $rpn);
                    // wait blocks until the query is done
                    yaz_wait();
                    if (yaz_error($session) != ""){
                        die("Error: " . yaz_error($session));
                    }
                    // yaz_hits returns the amount of found records
                    if (yaz_hits($session) > 0){
                        echo "Found some hits: ".yaz_hits($session)."<br/>";
                        // yaz_record fetches a record from the current result set,
                        // so far I've only seen server supporting string format
                        for ($i = 1; $i <= 20; $i++) {
                            $result = yaz_record($session, $i, "string; charset=marc-8,utf-8");
                            echo mb_detect_encoding($result, "auto");
                            print($result);
                            echo "<br><br>";
                            // the parsing functions will be introduced later
                            /*
                            if($syntax == "mab")
                                $parsedResult = parse_mab_string($result);
                            else
                                $parsedResult = parse_usmarc_string($result);
                             
                            
                            
                            print_r($parsedResult);
                             * */
                             
                        }
                    } else
                        echo "No records found.";
                }

            function parse_usmarc_string($record){
        $ret = array();
        // there was a case where angle brackets interfered
        $record = str_replace(array("<", ">"), array("",""), $record);
        //$record = utf8_decode($record);
        // split the returned fields at their separation character (newline)
        $record = explode("\n",$record);
        //examine each line for wanted information (see USMARC spec for details)
        foreach($record as $category){
            // subfield indicators are preceded by a $ sign
            $parts = explode("$", $category);
            // remove leading and trailing spaces
            array_walk($parts, "custom_trim");
            // the first value holds the field id,
            // depending on the desired info a certain subfield value is retrieved
            switch(substr($parts[0],0,3)){
                case "008" : $ret["language"] = substr($parts[0],39,3); break;
                case "020" : $ret["isbn"] = get_subfield_value($parts,"a"); break;
                case "022" : $ret["issn"] = get_subfield_value($parts,"a"); break;
                case "100" : $ret["author"] = get_subfield_value($parts,"a"); break;
                case "245" : $ret["titel"] = get_subfield_value($parts,"a");
                             $ret["subtitel"] = get_subfield_value($parts,"b"); break;
                case "250" : $ret["edition"] = get_subfield_value($parts,"a"); break;
                case "260" : $ret["pub_date"] = get_subfield_value($parts,"c");
                             $ret["pub_place"] = get_subfield_value($parts,"a");
                             $ret["publisher"] = get_subfield_value($parts,"b"); break;
                case "300" : $ret["extent"] = get_subfield_value($parts,"a");
                             $ext_b = get_subfield_value($parts,"b");
                             $ret["extent"] .= ($ext_b != "") ? (" : " . $ext_b) : "";
                             break;
                case "490" : $ret["series"] = get_subfield_value($parts,"a"); break;
                case "502" : $ret["diss_note"] = get_subfield_value($parts,"a"); break;
                case "700" : $ret["editor"] = get_subfield_value($parts,"a"); break;
            }
        }
        return $ret;
    }
     
    // fetches the value of a certain subfield given its label
    function get_subfield_value($parts, $subfield_label){
        $ret = "";
        foreach ($parts as $subfield)
            if(substr($subfield,0,1) == $subfield_label)
                $ret = substr($subfield,2);
        return $ret;
    }
     
    // wrapper function for trim to pass it to array_walk
    function custom_trim(& $value, & $key){
        $value = trim($value);
    }

?>
</body>
</html>