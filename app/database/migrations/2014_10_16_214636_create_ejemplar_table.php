<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEjemplarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ejemplar', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->string('no_adqui',15);
                        $table->bigInteger('ficha');
                        $table->bigInteger('idejemplar');
                        $table->bigInteger('ficha_no')->nullable();
                        $table->bigInteger('biblioteca');
                        $table->bigInteger('tipomaterial')->nullable();
                        $table->date('fechaingreso');
                        $table->bigInteger('volumen')->nullable();
                        $table->bigInteger('ejemplar')->nullable();
                        $table->bigInteger('tomo')->nullable();
                        $table->integer('accesible')->nullable();
                        $table->integer('escuela')->nullable();
                        $table->integer('revanio')->nullable();
                        $table->bigInteger('revvol')->nullable();
                        $table->bigInteger('revnum')->nullable();
                        $table->bigInteger('revejemplar')->nullable();
                        $table->string('revnotaejemplar',200)->nullable();

                        $table->primary(array('no_adqui','ficha','biblioteca'));
                        $table->foreign('biblioteca')->references('idbiblioteca')->on('biblioteca');
                        $table->foreign('tipomaterial')->references('idtipomaterial')->on('tipomaterial');
                        $table->foreign('ficha')->references('ficha')->on('idficha');
                        $table->foreign('escuela')->references('escuela')->on('idescuela');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('ejemplar'))
		Schema::drop('ejemplar');
	}

}
