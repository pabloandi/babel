<?php

Form::macro('jqRemoteLink',function($name, $options=array(), $html_options=array()){
    $html_options['onclick']=jq_remote_function($options);
    return HTML::link("#",$name,$html_options);
});

Form::macro('jqRemoteForm',function($options=array(), $html_options=array()){
    $html_options['onsubmit']=jq_remote_function($options)."; return false;";
    return Form::open($html_options);
});

Form::macro('jqRemoteFunction',function($options=array()){
    return jq_remote_function($options);
});


/**
 * Returns the javascript needed for a remote function.
 * Takes the same arguments as 'link_to_remote()'.
 *
 * Example:
 *   <select id="options" onchange="<?php echo remote_function(array('update' => 'options', 'url' => '@update_options')) ?>">
 *     <option value="0">Hello</option>
 *     <option value="1">World</option>
 *   </select>
 */
function jq_remote_function($options)
{
	// Defining elements to update
	if (isset($options['update']) && is_array($options['update']))
	{
		// On success, update the element with returned data
		if (isset($options['update']['success'])) $update_success = "#".$options['update']['success'];

		// On failure, execute a client-side function
		if (isset($options['update']['failure'])) $update_failure = $options['update']['failure'];
	}
	else if (isset($options['update'])) $update_success = "#".$options['update'];

	// Update method
	$updateMethod = _update_method(isset($options['position']) ? $options['position'] : '');

	// Callbacks
	if (isset($options['loading'])) $callback_loading = $options['loading'];
	if (isset($options['complete'])) $callback_complete = $options['complete'];
	if (isset($options['success'])) $callback_success = $options['success'];

	$execute = 'false';
	if ((isset($options['script'])) && ($options['script'] == '1')) $execute = 'true';

	// Data Type
	if (isset($options['dataType']))
	{
		$dataType = $options['dataType'];
	}
	elseif ($execute)
	{
		$dataType = 'html';
	}
	else
	{
		$dataType = 'text';
	}

	// POST or GET ?
	$method = 'POST';
	if ((isset($options['method'])) && (strtoupper($options['method']) == 'GET')) $method = $options['method'];

	// async or sync, async is default
	if ((isset($options['type'])) && ($options['type'] == 'synchronous')) $type = 'false';

	// Is it a form submitting
	if (isset($options['form'])) $formData = 'jQuery(this).serialize()';
	elseif (isset($options['submit'])) $formData = 'jQuery(\'#'.$options['submit'].'\').serialize()';
	// boutell and JoeZ99: 'with' should not be quoted, it's not useful
	// that way, see the Symfony documentation for the original remote_function
	elseif (isset($options['with'])) $formData = $options['with'];

	// build the function
	$function = "jQuery.ajax({";
	$function .= 'type:\''.$method.'\'';
	$function .= ',dataType:\'' . $dataType . '\'';
	if (isset($type)) $function .= ',async:'.$type;
	if (isset($formData)) $function .= ',data:'.$formData;
	if (isset($update_success) and !isset($callback_success)) $function .= ',success:function(data, textStatus){jQuery(\''.$update_success.'\').'.$updateMethod.'(data);}';
	if (isset($update_failure)) $function .= ',error:function(XMLHttpRequest, textStatus, errorThrown){'.$update_failure.'}';
	if (isset($callback_loading)) $function .= ',beforeSend:function(XMLHttpRequest){'.$callback_loading.'}';
	if (isset($callback_complete)) $function .= ',complete:function(XMLHttpRequest, textStatus){'.$callback_complete.'}';
	if (isset($callback_success)) $function .= ',success:function(data, textStatus){'.$callback_success.'}';
	//$function .= ',url:\''.URL::action($options['url']).'\'';
	$function .= ',url:\''.$options['url'].'\'';
	$function .= '})';

	if (isset($options['before']))
	{
		$function = $options['before'].'; '.$function;
	}
	if (isset($options['after']))
	{
		$function = $function.'; '.$options['after'];
	}
	if (isset($options['condition']))
	{
		$function = 'if ('.$options['condition'].') { '.$function.'; }';
	}
    if (isset($options['condition_else']))
    {
        $function = 'if ('.$options['condition_else'][0].') { '.$options['condition_else'][1].' } else { '.$function.'; }';
    }
	if (isset($options['confirm']))
	{
		$function = "if (confirm('".escape_javascript($options['confirm'])."')) { $function; }";
		if (isset($options['cancel']))
		{
			$function = $function.' else { '.$options['cancel'].' }';
		}
	}

	return $function;
}

function _update_method($position) {
	// Updating method
	$updateMethod = 'html';
	switch ($position) {
		case 'before':$updateMethod='before';break;
		case 'after':$updateMethod='after';break;
		case 'top':$updateMethod='prepend';break;
		case 'bottom':$updateMethod='append';break;
	}

	return $updateMethod;
}

/**
 * Escape carrier returns and single and double quotes for Javascript segments.
 */
function escape_javascript($javascript = '')
{
  $javascript = preg_replace('/\r\n|\n|\r/', "\\n", $javascript);
  $javascript = preg_replace('/(["\'])/', '\\\\\1', $javascript);

  return $javascript;
}



?>
