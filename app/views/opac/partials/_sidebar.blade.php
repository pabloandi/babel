
  <div id="sidebar"  class=" col-xs-6 col-sm-3 sidebar-offcanvas">
      
        <div   class='panel-group' id='acordeon-sidebar' role='tablist' aria-multiselectable='true'>
        
            <div clas='panel panel-default'>
                <div class='panel-heading' role='tab' id='sidebar-header-mejorarbusqueda' style="border-bottom: 1px solid #E8E8E8;">
                    <h4 class='panel-title'>
                        <a data-toggle='collapse' data-parent='#acordeon-sidebar' href='#sidebar-mejorarbusqueda' aria-expanded='true' aria-controls='sidebar-mejorarbusqueda'>
                            Redefinir búsqueda
                        </a>
                    </h4>
                </div>
                <div id='sidebar-mejorarbusqueda' class='panel-collapse collapse in' role='tabpanel' aria-labelledby='sidebar-header-mejorarbusqueda'>
                    
                        @if(isset($facetas))
                            @include('opac.partials._facetas',array('facetas'=>$facetas))
                        @endif
                    
                </div>
            </div>
            
            <div clas='panel panel-default'>
                <div class='panel-heading' role='tab' id='sidebar-header-tags'>
                    <h4 class='panel-title'>
                        <a data-toggle='collapse' data-parent='#acordeon-sidebar' href='#sidebar-tags' aria-expanded='true' aria-controls='sidebar-tags'>
                            Tags
                        </a>
                    </h4>
                </div>
                <div id='sidebar-tags' class='panel-collapse collapse in' role='tabpanel' aria-labelledby='sidebar-header-tags'>
                    <div class='panel-body'>
                        tags tags tags tags tags tags tags 
                    </div>
                </div>
            </div>
        
        </div>
      
  </div>
