
<div class="form-group">
    <label for="reserva-codigo-usuario">Código del usuario</label>
    <input type="text" class="form-control" id="reserva-codigo-usuario" name="reserva-codigo-usuario" placeholder="digite el número de cuenta"/>
</div>

<form id="form-solreserva-ejemplares">


    @if(isset($ejemplares) && count($ejemplares)>0)

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class='panel-title'>Ejemplares para solicitar reserva</h4>
                </div>
                <ul class='list-group'>
                    @foreach($ejemplares as $ejemplar)
                        <li class="list-group-item ejemplar" id="ejemplar-reservado-{{ trim($ejemplar->itemid) }}">
                            @include('opac.partials.ejemplares._ejemplar_reservado',array('ejemplar'=>$ejemplar))
                        </li>
                    @endforeach
                </ul>
            </div>

    @else
        No existen ejemplares reservados
    @endif

</form>
