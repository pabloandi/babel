<div class='media'>
    <div class='media-left hidden-xs'>
        <a href="#">
            <img class="media-object img-thumbnail" src="https://openclipart.org/image/64px/svg_to_png/137011/1304760779.png" alt="..." title="Simple question sign by  boobaloo (/user-detail/boobaloo)" />
        </a>
    </div>
    <div class="media-body small">
        <h4 class="media-heading">

        </h4>
        <div>
            <div class="row">
                <div class="col-md-6">
                    <strong>
                        Número de adquisición:
                    </strong>
                </div>
                <div class="col-md-6">
                    {{{ $ejemplar->itemid }}}
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <strong>
                        Biblioteca:
                    </strong>
                </div>
                <div class="col-md-6">
                    {{{ $ejemplar->agencyid }}}
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <strong>
                        Sede:
                    </strong>
                </div>
                <div class="col-md-6">
                    {{{ $ejemplar->itemagency }}}
                </div>

            </div>



    </div>
</div>
