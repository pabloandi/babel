<?php

return array(
    'siabuc'       =>      array(
        'nombre'    =>  'Biblioteca SIABUC9',
        'syntax'    =>  'usmarc',
        'ncip_url'  =>  'http://127.0.0.1:8000/siabuc-web-1.0/NCIPResponder'
    ),
    'koha'      =>      array(
        'nombre'    =>  'Biblioteca KOHA',
        'syntax'    =>  'usmarc',
        'ncip_url'  =>  'http://127.0.0.1:8001/koha-web-0.1/NCIPResponder'
    ),

)

?>
