<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reserva', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->integer('idreservacion');
                        $table->bigInteger('biblioteca');
                        $table->string('no_adqui',15);
                        $table->string('no_cuenta',20);
                        $table->date('fechafin');
                        $table->date('fechainicio');


                        $table->primary(array('idreservacion','biblioteca'));
                        $table->foreign('biblioteca')->references('idbiblioteca')->on('biblioteca');
                        $table->foreign('no_cuenta')->references('no_cuenta')->on('usuario');
                        $table->foreign('no_adqui')->references('no_adqui')->on('ejemplar');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('reserva'))
		Schema::drop('reserva');
	}

}
