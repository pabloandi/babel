$.templates({
    lookupitemset: `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                    <ns1:NCIPMessage xmlns:ns1="http://www.niso.org/2008/ncip" ns1:version="http://www.niso.org/schemas/ncip/v2_0/imp1/xsd/ncip_v2_0.xsd">
                        <ns1:Ext>
                            <ns1:LookupItemSet>
                            	<ns1:BibliographicId>
                                    <ns1:BibliographicRecordId>
                            			<ns1:BibliographicRecordIdentifier>{{:bibliographicrecordidentifier}}</ns1:BibliographicRecordIdentifier>
                            			<ns1:AgencyId>{{:agencyid}}</ns1:AgencyId>
                            		</ns1:BibliographicRecordId>
                            	</ns1:BibliographicId>
                            </ns1:LookupItemSet>
                        </ns1:Ext>
                    </ns1:NCIPMessage>`,
    lookupuser: `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                    <ns1:NCIPMessage xmlns:ns1="http://www.niso.org/2008/ncip" ns1:version="http://www.niso.org/schemas/ncip/v2_0/imp1/xsd/ncip_v2_0.xsd">
                        <ns1:LookupUser>
                            <ns1:UserId>
                                <ns1:UserIdentifierValue>{{:userid}}</ns1:UserIdentifierValue>
                            </ns1:UserId>
                        </ns1:LookupUser>
                    </ns1:NCIPMessage>`,
    createuser: `<ns1:NCIPMessage  xmlns:ns1="http://www.niso.org/2008/ncip" ns1:version="http://www.niso.org/schemas/ncip/v2_0/imp1/xsd/ncip_v2_0.xsd">
                  <ns1:CreateUser>
                    <ns1:UserId>
                        <ns1:AgencyId>{{:agencyid}}</ns1:AgencyId>
                        <ns1:UserIdentifierType>Barcode</ns1:UserIdentifierType>
                        <ns1:UserIdentifierValue>{{:useridentifiervalue}}</ns1:UserIdentifierValue>
                    </ns1:UserId>
                  <ns1:NameInformation>
                    <ns1:PersonalNameInformation>
                        <ns1:StructuredPersonalUserName>
                            <ns1:GivenName>{{:givenname}}</ns1:GivenName>
                            <ns1:Surname>{{:surname}}</ns1:Surname>
                        </ns1:StructuredPersonalUserName>
                    </ns1:PersonalNameInformation>
                  </ns1:NameInformation>
                  <ns1:UserAddressInformation>
                    <ns1:UserAddressRoleType>Home</ns1:UserAddressRoleType>
                    <ns1:ElectronicAddress>
                        <ns1:ElectronicAddressType>mailto</ns1:ElectronicAddressType>
                        <ns1:ElectronicAddressData>{{:electronicaddress}}</ns1:ElectronicAddressData>
                    </ns1:ElectronicAddress>
                  </ns1:UserAddressInformation>
                  <ns1:Ext></ns1:Ext>
                  </ns1:CreateUser>
                 </ns1:NCIPMessage>`,
    requestitem: `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                    <ns1:NCIPMessage  xmlns:ns1="http://www.niso.org/2008/ncip" ns1:version="http://www.niso.org/schemas/ncip/v2_0/imp1/xsd/ncip_v2_0.xsd">
                      <ns1:RequestItem>
                        <ns1:UserId>
                            <ns1:AgencyId>{{:agencyid}}</ns1:AgencyId>
                            <ns1:UserIdentifierValue>{{:userid}}</ns1:UserIdentifierValue>
                        </ns1:UserId>
                        <ns1:ItemId>
                            <ns1:AgencyId>{{:itemagency}}</ns1:AgencyId>
                            <ns1:ItemIdentifierValue>{{:itemid}}</ns1:ItemIdentifierValue>
                        </ns1:ItemId>
                        <ns1:RequestType ns1:Scheme="http://www.niso.org/ncip/v1_0/imp1/schemes/requesttype/requesttype.scm">Loan</ns1:RequestType>
                        <ns1:RequestScopeType ns1:Scheme="http://www.niso.org/ncip/v1_0/imp1/schemes/requestscopetype/requestscopetype.scm">Bibliographic Item</ns1:RequestScopeType>
                      </ns1:RequestItem>
                     </ns1:NCIPMessage>
                     `
});

window.info = function(message){
    $.notify({
        message: message
    },{
        type: 'info',
        z_index: 9999
    });
}

window.danger = function(message){
    $.notify({
        message: message
    },{
        type: 'danger',
        z_index: 9999
    });
}

function getLookupitemsetResult(endpoint,bibliographicrecordidentifier,agencyid){

    lookupitemset_data = {
        bibliographicrecordidentifier: bibliographicrecordidentifier,
        agencyid: agencyid
        };

        window.lookis=lookupitemset_data;

        jQuery.ajax({
            type: "POST",
            url: endpoint,
            cache: false,
            data:  $.templates.lookupitemset.render(lookupitemset_data),
            contentType: "text/xml",
            dataType: "text",
            crossDomain: true,
            processData: true,
            beforeSend: function(){
                info("Buscando ejemplares de la ficha seleccionada");
            },
            success: function(response) {

                xml=response.toString().replace(new RegExp('ns1:','g'),"");

                jQuery(xml).find('Problems').each(function(index, item){
                    danger(jQuery(item).find("ProblemDetail").text());
                    return false;
                });

                jQuery(xml).find('ItemInformation').each(function(index, item){
                    var itemid = jQuery(item).find("ItemIdentifierValue").text();
                    var itemagency = jQuery(item).find("AgencyId").text();
                    var circstatus = jQuery(item).find("CirculationStatus").text();


                    var row = document.createElement("div");
                    row.className = "row";

                    var columnid = document.createElement("div");
                    columnid.appendChild(document.createTextNode(itemid));
                    columnid.className = 'col-md-3';

                    var columnagency = document.createElement("div");
                    columnagency.appendChild(document.createTextNode(itemagency));
                    columnagency.className = 'col-md-3';

                    var columncirstatus = document.createElement("div");
                    columncirstatus.appendChild(document.createTextNode(circstatus));
                    columncirstatus.className = 'col-md-3';

                    row.appendChild(columnid);
                    row.appendChild(columnagency);
                    row.appendChild(columncirstatus);

                    if(circstatus == "Available On Shelf"){

                        var reserve_link = document.createElement('a');
                        reserve_link.href="javascript:void(0)";
                        reserve_link.onclick=function(){
                            var userid = jQuery("#data").data('usuario');

                            jQuery.ajax({
                                type: "POST",
                                url: "/opac/agregar-ejemplares",
                                data: {
                                    ejemplar_solicitado: {
                                        itemid: itemid,
                                        itemagency: itemagency,
                                        agencyid: agencyid,
                                        endpoint: endpoint
                                    }
                                },
                                cache: false,
                                success: function(){
                                    info("Ejemplar agregado al carrito");
                                },
                                error: function(){
                                    danger("Falló la agregación del ejemplar al carrito de solicitud de reserva");
                                }

                            });

                        }
                        reserve_link.innerHTML="Agregar al carrito";
                        row.appendChild(reserve_link);
                    }

                    jQuery("#fichas-ejemplares-listado").after(row);

                    $.notifyClose();
                 });
            }
        });


}

$.doLookupUser = function(endpoint, user_data){
    var dfd = $.Deferred();
    console.log(user_data);
     $.ajax({
        url: endpoint,
        type: "POST",
        cache: false,
        data: $.templates.lookupuser.render({userid: $.user_id}),
        contentType: "text/xml",
        dataType: "text",
        crossDomain: true,
        processData: false,
        beforeSend: function(){
            info("Buscando existencia del usuario en biblioteca");
        }
    }).done(function(response){
        xml=response.toString().replace(new RegExp('ns1:','g'),"");

        var problems = jQuery(xml).find("Problem");
        if(problems.length > 0){
            console.log("problem at looking up the user");
            if($(xml).find("ProblemType").text() == "Unknown User"){
                danger("No existe el usuario en la base de datos de la biblioteca");

            }
            else{
                danger(problems.find("ProblemDetail").text());
            }


            $.unknow_user_endpoints.push(endpoint);
            dfd.reject();
        }
        else{
            dfd.resolve();
        }
    }).fail(dfd.reject);

    return dfd.promise();
}

$.doCreateUser = function(endpoint, user_data){
        var dfd = $.Deferred();

        $.ajax({
            type: "POST",
            url: endpoint,
            cache: false,
            data: $.templates.createuser.render(user_data),
            contentType: "text/xml",
            dataType: "text",
            crossDomain: true,
            processData: true,
        }).done(function(response){
                xml=response.toString().replace(new RegExp('ns1:','g'),"");

                var problems = jQuery(xml).find("Problem");
                if(problems.length > 0){
                    danger(problems.find("ProblemDetail").text());
                    dfd.reject();
                }
                else{
                    dfd.resolve();
                }

        }).fail(dfd.reject);

        return dfd.promise();

}

$.doRequestItem = function(endpoint, reserve_data){

    // var reserve_data = {
    //         bibliograpicrecordidentifier: itemid,
    //         userid: userid,
    //         agencyid: agencyid
    // };

    var dfd = $.Deferred();

    jQuery.ajax({
        type: "POST",
        url: endpoint,
        cache: false,
        data: $.templates.requestitem.render(reserve_data),
        contentType: "text/xml",
        dataType: "text",
        crossDomain: true,
        processData: true,
        success: function(response) {
            var xml=response.toString().replace(new RegExp('ns1:','g'),"");
            var problems = jQuery(xml).find("Problem")

            if(problems.length > 0){
                danger(problems.find("ProblemDetail").text());

                dfd.reject(problems);
            }

            dfd.resolve(response);
        },
        error: function(response){
            dfd.reject(response);
        }

    });

    return dfd.promise();
}
