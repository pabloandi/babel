<?php

return array(
    "Libros" => array(
        "Monografías"   =>  "rt=a and bl=m",
        "Componente de Monografía"   =>  "rt=a and bl=a",
        "Colección"   =>  "rt=a and bl=c",
        "Subunidad"   =>  "rt=a and bl=d",
    ),
    "Revistas" => array(
        "Parte de componente serial"   =>  "rt=a and bl=m",
        "Recurso integrado"   =>  "rt=a and bl=i",
        "Serial"   =>  "rt=a and bl=s"
    ),
    "Música" => array(
        "Parte de componente serial"   =>  "rt=a and bl=m",
        "Recurso integrado"   =>  "rt=a and bl=i",
        "Serial"   =>  "rt=a and bl=s"
    ),
)
?>
