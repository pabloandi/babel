@extends('opac')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class='panel-title'>Mensaje desde el buzón de sugerencias</h4>
    </div>
    <div class="panel-body" role="form">
        <div class="form-group">
            <label for="nombre_usuario_contacto">Nombre del usuario</label>
            <p>
                {{{ $nombre }}}
            </p>
        </div>
        <div class="form-group">
            <label for="correo_usuario_contacto">Correo del usuario</label>
            <p>
                {{{ $correo }}}
            </p>
        </div>
        <div class="form-group">
            <label for="plan_usuario_contacto">Asunto del usuario</label>
            <p>
                {{{ $asunto }}}
            </p>
        </div>
        <div class="form-group">
            <label for="mensaje_usuario_contacto">Mensaje</label>
            <p>
                {{{ $mensaje }}}
            </p>
        </div>
    </div>
</div>

@stop