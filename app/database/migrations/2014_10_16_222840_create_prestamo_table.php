<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prestamo', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->integer('idprestamo');
                        $table->bigInteger('biblioteca');
                        $table->string('no_adqui',15);
                        $table->string('no_cuenta',20);
                        $table->integer('escuela');
                        $table->date('fechaprestamo');
                        $table->date('fechaentrega');
                        $table->string('tipoprestamo',3);
                        $table->string('usuario',50);
                        $table->string('titulo',150);
                        $table->string('autor',100);
                        $table->string('clasificacion',50);
                        $table->date('fechadevolucion')->nullable();
                        $table->smallInteger('tipomaterial');

                        $table->primary(array('idprestamo','biblioteca'));
                        $table->foreign('tipomaterial')->references('idtipomaterial')->on('tipomaterial');
                        $table->foreign('biblioteca')->references('idbiblioteca')->on('biblioteca');
                        $table->foreign('no_cuenta')->references('no_cuenta')->on('usuario');
                        $table->foreign('no_adqui')->references('no_adqui')->on('ejemplar');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('prestamo'))
		Schema::drop('prestamo');
	}

}
