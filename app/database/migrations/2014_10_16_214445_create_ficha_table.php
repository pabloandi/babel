<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ficha', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->bigInteger('idficha');
                        $table->bigInteger('ficha_no');
                        $table->bigInteger('biblioteca');
                        $table->bigInteger('tipomaterial');
                        $table->timestamp('fechacaptura')->nullable();
                        $table->string('tipoficha',1)->nullable();
                        $table->string('estatus',3)->nullable();
                        $table->text('titulo');
                        $table->text('autor');
                        $table->text('clasificacion');
                        $table->text('editorial');
                        $table->string('isbn',50)->nullable();
                        $table->string('lugarpub',50)->nullable();
                        $table->string('idiomapub',3)->nullable();
                        $table->string('fechapublicacion',50)->nullable();

                        $table->primary(array('idficha','ficha_no','biblioteca'));
                        $table->foreign('biblioteca')->references('idbiblioteca')->on('biblioteca');
                        $table->foreign('tipomaterial')->references('idtipomaterial')->on('tipomaterial');
		});
                
        DB::statement('alter table `ficha` add fulltext busqueda_ficha(titulo,autor,clasificacion,editorial,isbn,lugarpub,idiomapub)');
                
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('ficha')){
		Schema::drop('ficha');
                           
            }
	}

}
