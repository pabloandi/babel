<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuario', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->string('no_cuenta',20);
                        $table->string('nombre',50);
                        $table->integer('grupo');
                        $table->integer('escuela');
                        $table->string('correo',50)->nullable();
                        $table->string('domicilio',100)->nullable();
                        $table->string('colonia',30)->nullable();
                        $table->string('ciudad_estado',30)->nullable();
                        $table->string('telefono',50)->nullable();
                        $table->date('inicio_vigencia');
                        $table->date('fin_vigencia');
                        $table->string('nip',20)->nullable();

                        $table->primary('no_cuenta');
                        $table->foreign('escuela')->references('idescuela')->on('escuela');
                        $table->foreign('grupo')->references('no_grupo')->on('grupo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('usuario'))
		Schema::drop('usuario');
	}

}
