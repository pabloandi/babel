
<ul class='list-inline'>
    <li>
        <a id ="boton-reserva" data-loading-text ="<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>" href="#"  >
            <span class='hidden-xs'>Carrito</span>
            <span id='carrito-compra' class="glyphicon glyphicon-shopping-cart"></span>

        </a>
    </li>
    <li class="visible-xs">
        <a href='#'  data-toggle="offcanvas">
            <span class="glyphicon glyphicon-menu-right"></span>
        </a>
    </li>

</ul>
