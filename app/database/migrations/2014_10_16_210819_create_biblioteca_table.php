<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBibliotecaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('biblioteca', function(Blueprint $table)
		{
			 $table->engine = 'MyISAM';

                        $table->bigInteger('idbiblioteca');
                        $table->string('nombre',250);
                        $table->string('nombrecorto',50)->nullable();
                        $table->string('director',60)->nullable();
                        $table->string('telefono',100)->nullable();
                        $table->string('correo',100)->nullable();
                        $table->tinyInteger('activo')->default(1);
                        $table->tinyInteger('version')->default(9);

                        $table->primary('idbiblioteca');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('biblioteca'))
		Schema::drop('biblioteca');
	}

}
