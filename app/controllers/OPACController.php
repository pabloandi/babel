<?php

class OPACController extends \BaseController {

    protected $layout='opac';
    protected $metaproxy;


    // function __construct()
    // {
    //     // create a client instance
    //     $this->beforeFilter(function(){
    //         try {
    //             $this->metaproxy=yaz_connect('metaproxy:9000',array('charset'=>'UTF-8'));
    //         }
    //         catch (Exception $e){
    //
    //         }
    //     });
    //
    // }

     /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
                return View::make('opac.index');
	}

        /** Consulta de información
         *
         */
        public function getConsultar(){
            $pagina=Input::get('page',1);
            $limite=Input::get('limite',10);

            $categoria=Input::get('categoria','libre');
            $filtros=Input::get('filtros');
            $bibliotecas=Input::get('bibliotecas','all');
            $ordenar=Input::get('ordenar',array('score'=>'desc'));
            $q=Input::get('q');

            $parametros=array(
                    'filtros'         =>      $filtros,
                    'bibliotecas'     =>      $bibliotecas,
                    'categoria'       =>      $categoria,
                    'ordenar'         =>      $ordenar,
                    'limite'         =>       $limite,
                    'q'               =>      $q
            );


            // $fichas = Cache::remember('consulta:'.serialize(array_add($parametros,'page',$pagina)), 500,
            //         function() use ($parametros,$pagina){
            //
            //     $metaproxy = App::make('metaproxy');
            //     return Busquedas::buscarZ3950($metaproxy, $parametros, $pagina);
            // });

            $metaproxy = App::make('metaproxy');
            $fichas = Busquedas::buscarZ3950($metaproxy, $parametros, $pagina);

            // dd($fichas);

            $pager = Paginator::make( $fichas['fichas'], $fichas['hits'], $limite );
            $pager->appends($parametros);

            $response = array(
                'datos' => View::make( 'opac.consulta', array('pager' => $pager))->render()
            );


            return Response::json ( $response );

        }

        public function getBusquedaAvanzada(){

            $bibliotecas = Cache::remember('bibliotecas',500,function(){
                return DB::table('biblioteca')->where('activo',1)->get();
            });

            $tiposmaterial = Cache::remember('tiposmaterial',500,function(){
                return DB::table('tipomaterial')->get();
            });

            return View::make('opac.partials._busqueda_avanzada',array(
                'bibliotecas'   =>  $bibliotecas,
                'tiposmaterial'    =>  $tiposmaterial,
            ));


        }

        public function getMostrarFichaEjemplares(){
            if(Input::has("fichas")){


                return View::make('opac.fichaejemplares');

            }
            else { App::abort(500,'No hay fichas que mostrar'); }
        }



        public function getContacto(){
            $bibliotecas = Cache::remember('bibliotecas',500,function(){
                    return DB::table('biblioteca')->where('activo',1)->get();
                });

            return View::make('opac.contacto',array('bibliotecas'=>$bibliotecas));
        }

        public function postEnviarCorreoContacto(){
            $mensaje=Input::get('mensaje_usuario_contacto');
            $nombre=Input::get('nombre_usuario_contacto');
            $correo=Input::get('correo_usuario_contacto');
            $asunto=Input::get('plan_usuario_contacto');

            $biblioteca=  Biblioteca::find(Input::get('biblioteca_contacto'));

            Mail::later(20,'emails.buzonsugerencias',array('nombre'=>$nombre, 'correo'=>$correo, 'asunto'=>$asunto, 'mensaje'=>$mensaje), function($message){
                            $message->to($biblioteca->correo, $biblioteca->nombre)->subject('Solicitud de reserva de material');
                        });

        }

	public function getAyuda(){
             return View::make('opac.ayuda');
        }

        public function getInfoUsuario(){
            $codigo=Input::get("codigo_usuario");

            try{

                $usuario=Usuario::with('laescuela','elgrupo')->where('no_cuenta','=',$codigo)->firstOrFail();
            }
            catch(Exception $e){
                 App::abort(500,'No existe el usuario');
            }



            return View::make('opac.infousuario',array('usuario'=>$usuario));

        }

        public function getEjemplaresReservados(){
            $ejemplares_reservados=array();
            if(Session::has('ejemplares_reservados')){
                $ejemplares_reservados=Session::get('ejemplares_reservados');
                // dd($ejemplares_reservados);
            }

            return View::make('opac.ejemplaresreservados',array('ejemplares'=>$ejemplares_reservados));
        }

        public function getEnviarSolicitudReserva(){
            $usuario=Usuario::find(Input::get('reserva_codigo_usuario'));
            if(!$usuario)
                App::abort(500,'Usuario no existente');

            $ejemplares=  Ejemplar::find(explode("_",Input::get('ejemplares_a_reservar')));

            if(count($ejemplares)>0){
                $ejes=array();
                foreach ($ejemplares as $ejemplar){
                    $ejes[$ejemplar->biblioteca->correo][]=$ejemplar;
                }

                foreach(array_keys($ejes) as $biblioteca){

                        Mail::later(20,'emails.solicitudreserva',array('usuario'=>$usuario, 'ejemplares'=>$ejes[$biblioteca]), function($message){
                            $message->to($biblioteca, $ejes[$biblioteca]->biblioteca->nombre)->subject('Solicitud de reserva de material');
                        });

                }
            }
            else
                App::abort(500,'No hay ejemplares que enviar');

        }


        public function postAgregarEjemplares(){
            $ejemplar_solicitado=Input::get("ejemplar_solicitado");

            // $ejemplar = array();
            // $ejemplar[$ejemplar_solicitado['itemid']] = $ejemplar_solicitado;

            Session::push('ejemplares_reservados',(object) $ejemplar_solicitado);

            // if(is_array($ejemplares)){
            //     foreach ($ejemplares as $ejemplar)
            //         $ejemplares=Session::push('ejemplares_reservados',$ejemplar);
            // }
            // else
            //     $ejemplares=Session::push('ejemplares_reservados',$ejemplares);

            return 'ok';

        }

        public function getRechazarEjemplar(){

            $ejs = array();
            $ejemplares=Session::get('ejemplares_reservados');
            $ejemplar_rechazado=Input::get("ejemplar_rechazado");


            foreach ($ejemplares as $ejemplar) {
                if(trim($ejemplar->itemid) != trim($ejemplar_rechazado))
                    $ejs[]=$ejemplar;
            }

            Session::put('ejemplares_reservados', $ejs);

            return Response::json(['ejemplares' => $ejs, 'ejemplar_rechazado' => $ejemplar_rechazado]);

        }


        public function getBuscarItemsPorClasificacion(){

            $biblioteca=Input::get("biblioteca");
            $clasificacion=Input::get("clasificacion");

            if(!$clasificacion || !$biblioteca){
                App::abort(500,"Se debe proveer la clasificación de la ficha para buscar los items");
            }

            $bibliotecas=Config::get("bibliotecas");

            $conn = new NcipConnector($bibliotecas[$biblioteca]['ncip_url'],$biblioteca,"babel");
            $client = NcipClient($conn);

            $client = App::make('ncip.client');

        }

        public function getTest(){

        }

        public function getFlushSession(){
            Session::flush();
            if(Session::has('ejemplares_reservados')){
                return 'fail';
            }
            else {
                return 'ok';
            }

        }


}
