<?php

class Busquedas {


    static public function buscarZ3950($conexion,$parametros,$pagina){

            if(is_array($parametros['bibliotecas']))
                $bd=join("+", $parametros['bibliotecas']);
            if($parametros['bibliotecas']=='all')
                $bd=join("+", array_keys(Config::get('bibliotecas')));
            else
                $bd=$parametros['bibliotecas'];

            //TODO arreglar base de datos entera
            //$bd='koha';
            // dd($bd);

            if(!yaz_database($conexion,$bd)){
                App::abort(500,"No se pudo conectar a las base de datos");
            }

            yaz_syntax($conexion, 'usmarc');
            yaz_ccl_conf($conexion, Config::get('cclmaping'));

            try{
                $consulta=  self::makeQueryZ3950($parametros);
                //$consulta=  "(titulo = cien) and (autor = gabriel)";
                yaz_ccl_parse($conexion,$consulta,$ccl_result);
            }
            catch(Exception $e){
                App::abort(500,sprintf("No se pudo transformar la consulta suministrada -> %s:%s",yaz_errno($conexion),yaz_error($conexion)));
            }

            $rpn=$ccl_result['rpn'];
            $paginacion_inicio=($pagina * $parametros['limite']) - $parametros['limite'];
            $paginacion_inicio=($paginacion_inicio==0) ? 1 : $paginacion_inicio;

            yaz_range($conexion, $paginacion_inicio, $parametros['limite']);
            yaz_search($conexion,'rpn',$rpn);
            $opciones=array('timeout'=>15);
            yaz_wait($opciones);

            if(yaz_error($conexion) != ""){
                App::abort(500,sprintf("Hubo un error en la consulta -> %s:%s (%s)",yaz_errno($conexion),yaz_error($conexion),$rpn));
            }

            $fichas=array();
            $hits=yaz_hits($conexion);



            if($hits>0){


                // for ($i = $paginacion_inicio; $i <= ($paginacion_inicio+$parametros['limite']); $i++) {
                for ($i = 1; $i <= $hits; $i++) {
                    $ficha=array();
                    $ficha['id']=rand(1, 2000);
                    $ficha['etiquetasmarc']=self::parse_usmarc_string(yaz_record($conexion,$i,"string; charset=marc-8,utf-8"));
                    $ficha['biblioteca']=  Config::get(sprintf('bibliotecas.%s.nombre',yaz_record($conexion,$i,"database")));
                    $ficha['ncip']=  Config::get(sprintf('bibliotecas.%s.ncip_url',yaz_record($conexion,$i,"database")));

                    $ficha['titulo']=(isset($ficha['etiquetasmarc']['titulo'])) ? $ficha['etiquetasmarc']['titulo'] : null;
                    $ficha['autor']=(isset($ficha['etiquetasmarc']['autor'])) ? $ficha['etiquetasmarc']['autor'] : null;
                    $ficha['clasificacion']=(isset($ficha['etiquetasmarc']['clasificacion'])) ? $ficha['etiquetasmarc']['clasificacion'] : null;
                    $ficha['isbn']=(isset($ficha['etiquetasmarc']['isbn'])) ? $ficha['etiquetasmarc']['isbn'] : null;
                    $ficha['cn']=(isset($ficha['etiquetasmarc']['cn'])) ? $ficha['etiquetasmarc']['cn'] : null;
                    $fichas[]=(object) $ficha;

                }

            }



            //$close=yaz_close($conexion);

            return array('fichas'   =>  $fichas, 'hits' =>  $hits   );
    }

    static public function parse_usmarc_string($record){
        $ret = array();
        // there was a case where angle brackets interfered
        $record = str_replace(array("<", ">"), array("",""), $record);
        //$record = utf8_decode($record);
        // split the returned fields at their separation character (newline)
        $record = explode("\n",$record);
        //examine each line for wanted information (see USMARC spec for details)
        foreach($record as $category){
            // subfield indicators are preceded by a $ sign
            $parts = explode("$", $category);
            // remove leading and trailing spaces
            array_walk($parts, function(&$value, $key){
                $value = trim($value);
            });
            // the first value holds the field id,
            // depending on the desired info a certain subfield value is retrieved
            switch(substr($parts[0],0,3)){
                case "001" : $ret["cn"] = substr($parts[0],4); break;
                case "008" : $ret["lenguaje"] = substr($parts[0],39,3); break;
                case "020" : $ret["isbn"] = self::get_subfield_value($parts,"a"); break;
                case "022" : $ret["issn"] = self::get_subfield_value($parts,"a"); break;
                case "090" : $ret["catalogacion"] = self::get_subfield_value($parts,"a"); break;
                case "100" : $ret["autor"] = self::get_subfield_value($parts,"a"); break;
                case "245" : $ret["titulo"] = self::get_subfield_value($parts,"a");
                             $ret["subtitulo"] = self::get_subfield_value($parts,"b"); break;
                case "250" : $ret["edicion"] = self::get_subfield_value($parts,"a"); break;
                case "260" : $ret["fecha_pub"] = self::get_subfield_value($parts,"c");
                             $ret["lugar_pub"] = self::get_subfield_value($parts,"a");
                             $ret["publicador"] = self::get_subfield_value($parts,"b"); break;
                case "300" : $ret["extent"] = self::get_subfield_value($parts,"a");
                             $ext_b = self::get_subfield_value($parts,"b");
                             $ret["extent"] .= ($ext_b != "") ? (" : " . $ext_b) : "";
                             break;
                case "490" : $ret["series"] = self::get_subfield_value($parts,"a"); break;
                case "502" : $ret["diss_note"] = self::get_subfield_value($parts,"a"); break;
                case "700" : $ret["editor"] = self::get_subfield_value($parts,"a"); break;
            }
        }
        return $ret;
    }

    // fetches the value of a certain subfield given its label
    static protected function get_subfield_value($parts, $subfield_label){
        $ret = "";
        foreach ($parts as $subfield)
            if(substr($subfield,0,1) == $subfield_label)
                $ret = substr($subfield,2);
        return $ret;
    }

    static public function makeQueryZ3950($parametros){
        extract($parametros);
        $q=sprintf('%s="%s',$categoria,$q);

        if(count($filtros)>0){
             foreach($filtros as $operador => $campovalor){
                list($op,$c)=explode(':',$operador);
                if(is_array($campovalor)){


                    foreach ($campovalor as $valor){

                        $q.=sprintf(' %s %s="%s"', $op,$c,$valor);
                    }

                }
                else{
                    list($op,$c)=explode(':',$operador);
                    $q.=sprintf(' %s %s:"%s"', $op,$c,$campovalor);

                }

            }
        }

        return $q;
    }
}
?>
