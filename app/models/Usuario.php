<?php

class Usuario extends Eloquent{
    
    protected $table='usuario';


    public function laescuela(){
        return $this->hasOne('Escuela','idescuela','escuela');
    }
    
    public function elgrupo(){
        return $this->hasOne('Grupo','no_grupo','grupo');
    }
    
    public function prestamos(){
        return $this->hasMany('Prestamo','no_cuenta','no_cuenta');
    }
    
    public function multas(){
        return $this->hasMany('Multa','no_cuenta','no_cuenta');
    }
    
    public function totalmultas(){
        $totalMulta=0;

        $multas= $this->multas()->get();
        

        if(count($multas)>0){
            foreach ($multas as $multa)
                $totalMulta+=(int) $multa->monto;

        }
        
        return number_format($totalMulta,0,'.','');
    }
}

?>
